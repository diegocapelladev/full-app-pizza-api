import path from 'node:path'
import express from 'express'
import * as dotenv from 'dotenv'
import mongoose from 'mongoose'

import { router } from './router'

dotenv.config()
const app = express()
const PORT = process.env.PORT || 3001

mongoose.connect('mongodb://localhost:27017')
  .then(() => {
    app.use('/uploads', express.static(path.resolve(__dirname, '..', 'uploads')))
    app.use(express.json())
    app.use(router)

    app.listen(PORT, () => {
      console.log(`Server is running on http://localhost:${PORT}`)
    })
  })
  .catch(() => console.log('Error to connect'))



